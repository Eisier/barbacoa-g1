﻿== ﻿Lista de asistentes

// Ordenados por orden alfabético
// Formato: Apellidos, Nombre

* Ayuso Martínez, Álvaro
* Chura Pascual, Albaro
* Herreria Oña, Asier
* Juan Chico, Jorge
* Moreno Monrobe, Alberto

=== Organización del transporte

// Si tienes vehículo, pon el número de plazas. Si no tienes, añádete a
// alguno de los vehículos existentes.

==== Moto de Jorge (2 plazas)

* Ayuso Martínez, Álvaro
* Jorge Juan-Chico

==== Coche de Alberto (4 plazas)

* Moreno Monrobe, Alberto

==== Ala delta de Asier (1 plazas)

* Herreria Oña, Asier
